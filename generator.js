function worker(gen, arg) {
  const generator = gen.next ? gen : gen();

  let currentVal = generator.next(arg);

  if (currentVal.done) {
    return currentVal.value;
  }

  if (
    typeof currentVal.value === 'function' &&
    typeof currentVal.value().next === 'function'
  ) {
    const func = currentVal.value();
    return Promise.resolve(func).then(item => worker(item, item.next));
  }

  if (typeof currentVal.value.next === 'function') {
    return Promise.resolve(worker(currentVal.value)).then(item =>
      worker(generator, item)
    );
  }

  return Promise.resolve(currentVal.value).then(item =>
    worker(generator, item)
  );
}
